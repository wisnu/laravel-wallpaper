<?php

namespace App\Http\Controllers;

use HTMLMin\HTMLMin\HTMLMin;
use HTMLMin\HTMLMin\Minifiers\BladeMinifier;
use HTMLMin\HTMLMin\Minifiers\CssMinifier;
use HTMLMin\HTMLMin\Minifiers\HtmlMinifier;
use HTMLMin\HTMLMin\Minifiers\JsMinifier;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;

use Illuminate\Support\Facades\Storage; 
use Illuminate\Support\Facades\View;

use Response;
use Cookie;
use Illuminate\Http\Request;

class Controller extends BaseController
{
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
	public function __construct()
	{
		View::share('money', Config::get('money'));
	}


	/* *
	 *	Get homepage using keywords.txt file
 	 *
 	 */
	public function index() {
		$lines = file(Storage::disk('local')->url('keywords.txt'), FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
		shuffle($lines);
		$result = array_slice($lines, 0, 900);

		if (Cache::has('index_related')) {
			$related = Cache::get('index_related');
		} else {			
			$related = Cache::remember('index_related', 10, function () use ($lines) {
				shuffle($lines);
				return array_slice($lines, 1000, 21);
			});
		}



		return view('index')
		->with('result', $result)
		->with('related', $related)
		;
    
    }



	/* *
	 *	Render category page
 	 *
 	 *	@param string $slug
 	 */
	public function single($slug, $page=1) {
		$key = str_replace('-', ' ', $slug);
		$cachekey = md5($key);
		$lines = file(Storage::disk('local')->url('keywords.txt'), FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
		shuffle($lines);

		if (Cache::has($cachekey.'.json')) {
        	$result = Cache::get($cachekey.'.json');
        } else {
            $result = file_get_contents('http://pool.wis.nu/wallpaper/tes2/'.urlencode($key));
          	Cache::put($cachekey.'.json', $result, 1440);
            $result = Cache::get($cachekey.'.json');
        }
      
		if (Cache::has('related')) {
			$related = Cache::get('related');
		} else {			
			$related = Cache::remember('related', 10, function () use ($lines) {
				return array_slice($lines, 1000, 36);
			});
		}

		if (Cache::has('related2')) {
			$related2 = Cache::get('related2');
		} else {			
			$related2 = Cache::remember('related2', 10, function () use ($lines) {
				return array_slice($lines, 0, 60);
			});
		}

		return view('single')
		->with('page', $page)
		->with('title', ucwords(str_replace('-', ' ', $slug)))
		->with('result', $result)
		->with('related', $related)
		->with('related2', $related2)
	//	->render()
		;
	}


	/* *
	 *	Render single page
 	 *
 	 *	@param string $slug
 	 *	@param string $permalink (free to change)
 	 *	@param string $id 	// base64_encode with substr (__,0,18)
 	 */
	public function attachment($slug, $permalink) {
		$key = str_replace('-', ' ', $slug);
		$cachekey = md5($key);
		$lines = file(Storage::disk('local')->url('keywords.txt'), FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

      	if (Cache::has($cachekey.'.json')) {
        	$result = Cache::get($cachekey.'.json');
        } else {
            $result = file_get_contents('http://pool.wis.nu/wallpaper/tes2/'.urlencode($key));
          	Cache::put($cachekey.'.json', $result, 1440);
            $result = Cache::get($cachekey.'.json');
         
        }
      
      
		$resultArr = json_decode($result, true);
		$result = json_decode($result);

		$valNum = array_search($permalink, array_column($resultArr['data'], 'slug'));

		if (Cache::has('related')) {
			$related = Cache::get('related');
		} else {			
			$related = Cache::remember('related', 10, function () use ($lines) {
				shuffle($lines);
				return array_slice($lines, 1000, 36);
			});
		}

		if (Cache::has('related2')) {
			$related2 = Cache::get('related2');
		} else {			
			$related2 = Cache::remember('related2', 10, function () use ($lines) {
				shuffle($lines);
				return array_slice($lines, 0, 60);
			});
		}

		return view('attachment')
		->with('id', $valNum)
		->with('slug', $slug)
		->with('result', $result)
		->with('resultArr', $resultArr)
		->with('related', $related)
		->with('related2', $related2)
		->with('suggestion', @implode(' ', $this->suggestions(str_replace('-', ' ', $slug))))
		->render()
		;

	}



	/* *
	 *	Static page.
 	 *
 	 *	@param string $slug

 	 */
	public function page($slug) {
		if ($slug == 'privacy-policy') {
			$pageName = 'Privacy Policy';
		} elseif ($slug == 'dmca') {
			$pageName = 'DMCA';
		} elseif ($slug == 'toc') {
			$pageName = 'Terms of Conditions';
		} elseif ($slug == 'contact') {
			$pageName = 'Contact Us';
		}

		return view('page')
		->with('page', $pageName)
		->with('content', Config::get("themes.$slug"))
//		->with('related', array_slice($this->data, 0, 10))
		->render();

	}



	/* *
	 *	Helper
 	 *
 	 */
	public function suggestions($slug) {
		$relatedGo = json_decode(@file_get_contents("http://suggestqueries.google.com/complete/search?client=firefox&q=$slug&hl=en"));
		if ($relatedGo === FALSE) {
			$related = '';
			return $related;
		} elseif ($relatedGo === TRUE){
			$related = array();
			foreach ($relatedGo[1] as $r1) {
				$related[] = ucfirst($r1);
			}
	
			return $related;
		}
	}

}



