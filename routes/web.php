<?php

Route::get('/{page?}', 'Controller@index')->name('home')->where('page', '[1-9]+[0-9]*');
Route::get('{slug}/{page?}', 'Controller@single')->where('page', '[1-9]+[0-9]*');
Route::get('page/{slug}.html', 'Controller@page');
Route::get('{slug}/{permalink}.html', 'Controller@attachment');






