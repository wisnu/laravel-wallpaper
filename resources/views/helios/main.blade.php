<!DOCTYPE html>

<html>
<head>
	<title>
		@yield('title')
	</title>
	<meta content="text/html; charset=utf-8" http-equiv="content-type">
	<meta content="index,follow,imageindex" name="googlebot">
	<meta content="all,index,follow" name="robots">
	<meta content="all,index,follow" name="googlebot-Image">
	<meta content="width=device-width,initial-scale=1" name="viewport">
	<meta content="general" name="rating">
	<meta content="global" name="distribution">
	<meta content="en" name="language">
	<meta content="#FF6600" name="msapplication-TileColor">
	<meta content="{{ url('/') }}" name="application-name">
	<meta content="{{ url('/') }}" name="msapplication-tooltip">
	<meta content="{{ theme_url('icon.png') }}" name="msapplication-TileImage" title="favicon ie10">
	@yield('meta')
	<link href="{{ theme_url('style.css') }}" id="simpress_main_style-css" rel="stylesheet">
	<link href="{{ theme_url('favicon.ico') }}" rel="shortcut icon" type="image/x-icon">
	<link href="{{ theme_url('icon.png') }}" rel="apple-touch-icon">
	<link href="{{ theme_url('icon.png') }}" rel="icon" sizes="57x57">
</head>

<body>
	<div id="kp">
		<div class="kr">
			{{ ucwords(Config::get('money.niche')) }}
		</div>


		<div class="kn">
			<ul class='menu'>
				<li>
					<a href="{{ url('page/contact.html') }}" title="contact">Contact</a>
				</li>


				<li>
					<a href="{{ url('page/privacy-policy.html') }}" title="privacy">Privacy</a>
				</li>


				<li>
					<a href="{{ url('page/toc.html') }}" title="terms">Terms</a>
				</li>


				<li>
					<a href="{{ url('page/dmca.html') }}" title="DMCA">DMCA</a>
				</li>
			</ul>
		</div>

@yield('content')

	<div class="bf">
		Copyright © {{date('Y')}} {{ ucwords(Config::get('money.niche')) }}. Some Rights Reserved.
	</div>
   <!-- Histats.com  START  (aync)-->
<script type="text/javascript">var _Hasync= _Hasync|| [];
_Hasync.push(['Histats.start', '1,3915070,4,0,0,0,00010000']);
_Hasync.push(['Histats.fasi', '1']);
_Hasync.push(['Histats.track_hits', '']);
(function() {
var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
hs.src = ('//s10.histats.com/js15_as.js');
(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
})();</script>
<noscript><a href="/" target="_blank"><img  src="//sstatic1.histats.com/0.gif?{{ $money['trackid'] }}&101" alt="" border="0"></a></noscript>
<!-- Histats.com  END  -->
</body>
</html>