@extends('main')
@section('title')
	{{ ucwords(Config::get('money.niche')) }}
@endsection

@section('meta')
	<meta name="google-site-verification" content="{{ Config::get('money.metaverification')}}" />
	<link href='{{ url()->current() }}' rel='canonical'>
	<meta name="description" content="{{ implode(', ', array_slice($related, 0, 10)) }}">
	<meta name="keywords" content="{{ implode(', ', array_slice($related, 0, 10)) }}">
@endsection


@section('content')
		<div class="cl">
		</div>
	</div>


	<div id='cc'>
		<div class="crumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
			<span typeof="v:Breadcrumb"><a href="/" property="v:title" rel="v:url">Home</a></span>
		</div>


		<div id="cl">
			<div class="content">
				<h1 class="ld">contact</h1>
				<p><?=$content;?></p>

			</div>
		</div>
		<div id="sb">
			<div class="cl">
			</div>


			<h3 class="hc">Random post:</h3>


			<ul class="rand-text">
				@foreach ($related as $rel)
				<li>
					<h3>
					<a href="{{ url(str_slug($rel)) }}" title="{{ ucwords($rel) }}" rel="bookmark">{{ ucwords($rel) }}</a>
					</h3>
				</li>

				@endforeach

			</ul>


			<div class="cl">
			</div>
		</div>


		<div class="cl">
		</div>


	</div>
@endsection