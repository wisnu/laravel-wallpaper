@extends('layout')


@section('meta')
    <meta content="Office Design and Remodel Inspiration - sqst.space" name="description">
    <meta content="home interior,cool home interior,teens room,awesome living room designs,stunning bedroom designs,contemporary bathroom designs,family room,kitchen design,bedroom design,kids room,baby rooms,amazing home interior design ideas,cool wall decorations,amazing furniture set,home interior designs,interior home inspirations" name="keywords">
@endsection

@section('content')
        <div id="hub">
            <a href="http://sqst.space"><span></span></a>

            <div class="menu-hub">
                <div class="dropdown">
                    <div id="breadchumb">
                        <a href="http://sqst.space">Home</a>
                    </div>
                </div>
            </div>
        </div>


        <div id="container">
            <div id="postal">

                @foreach ($result as $data)
                <div class="posta">
                    <div class="posa"><img alt="Stunning 50 Jwt New York Office Design Decoration Of Jwt&#039;s New Advertising Office Office" class="lazy-hidden attachment-featured-homes size-featured-homes" data-lazy-type="image" data-src="http://sqst.space/wp-content/uploads/2017/09/stunning-50-jwt-new-york-office-design-decoration-of-jwts-new-advertising-office-office-snapshots-236x130.jpg" height="130" src="//sqst.space/wp-content/plugins/a3-lazy-load/assets/images/lazy_placeholder.gif" title="Stunning 50 Jwt New York Office Design Decoration Of Jwt&#039;s New Advertising Office Office" width="236">
                    </div>


                    <div class="posi">
                        <h2><a href="{{ url('/'.str_slug($data)) }}" title="{{ substr(ucwords(str_replace('-', ' ', $data)), 0, 30) }}">{{ ucwords($data) }}</a>
                        </h2>
                    </div>
                </div>
                @endforeach




                <div class="clear">
                </div>


                <div id="post-navigator">
                    <div class="wp-pagenavi">
                        <span class="pages">Page 1 of 167:</span><strong class='current'>1</strong> <a href="http://sqst.space/page/2/">2</a> <a href="http://sqst.space/page/3/">3</a> <a href="http://sqst.space/page/4/">4</a> <a href="http://sqst.space/page/5/">5</a> <a href="http://sqst.space/page/6/">6</a> <a href="http://sqst.space/page/7/">7</a> <a href="http://sqst.space/page/8/">8</a> <a href="http://sqst.space/page/9/">9</a> <a href="http://sqst.space/page/10/">10</a> <a href="http://sqst.space/page/11/">11</a> <a href="http://sqst.space/page/12/">12</a> <a href="http://sqst.space/page/13/">13</a> <a href="http://sqst.space/page/14/">14</a> <a href="http://sqst.space/page/15/">15</a> <a href="http://sqst.space/page/2/"><strong>&raquo;</strong></a>&nbsp;<a href="http://sqst.space/page/167/">Last &raquo;</a>
                    </div>


                    <div class="clearfix">
                    </div>
                </div>
            </div>
        </div>


        <div id="side">
            <div class="menu">
                <form action="http://sqst.space/" method="get">
                    <input class="search" name="s" onblur="if(this.value == '') this.value='';" onclick="if(this.value == '') this.value='';" title="Hit enter to search" type="text" value="">
                </form>
            </div>


            <div class="clear">
            </div>


            <div class="hcat">
                Popular
            </div>


            <div class="popularpost">

                @foreach ($related as $rel)

                <div class="pop">
                    <div class="infothumb">
                        <a href="{{ url(str_slug($rel)) }}" title="Download {{ ucwords($rel) }}">{{ ucwords($rel) }}</a>
                    </div>
                </div>

                @endforeach


            </div>


            <div class="clear">
            </div>


            <div class="hcat">
                Category
            </div>


            <div class="popularpost">
                <ul>
                    <li class="cat-item cat-item-1">
                        <a href="http://sqst.space/category/office/">Office</a>
                    </li>
                </ul>
            </div>


            <div class="adside">
            </div>
        </div>


        <div class="clear">
        </div>
    </div>
@endsection
