<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="vqlhu_OIx_KkhxSfa4ENw4CiSwnEHxObdZK6m25Qlnc" name="google-site-verification">
    <meta content='index,follow,imageindex' name='googlebot'>
    <meta content='index,follow' name='robots'>
    <meta content='all,index,follow' name='googlebot-Image'>

    <title>Office Design Ideas | Office Design and Remodel Inspiration</title>
@yield('meta')
    <meta content="Office Design Ideas | sqst.space" property="og:title">
    <meta content="Office Design and Remodel Inspiration - sqst.space" property="og:description">
    <meta content="sqst.space" property="og:site_name">
    <meta content="website" property="og:type">
    <meta content="image/jpeg" property="og:image:type">
    <link href="http://sqst.space/" rel="canonical">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <link href="http://sqst.space/wp-content/themes/sqst/images/favicon.ico" rel="shortcut icon">
    <script type="text/javascript">
    //<![CDATA[
    if (top.location != location)
    top.location.href = document.location.href ;
    //]]>
    </script>
    <link href="http://sqst.space/wp-content/themes/sqst/style.css" rel="stylesheet">
    <link href="http://sqst.space/feed/" rel="alternate" title="Office Design Ideas RSS Feed" type="application/rss+xml">
    <link href="http://sqst.space/xmlrpc.php" rel="pingback">
    <link href='//s.w.org' rel='dns-prefetch'>
    <script type="text/javascript">
               window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.3\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.3\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/sqst.space\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.8.2"}};
               !function(a,b,c){function d(a){var b,c,d,e,f=String.fromCharCode;if(!k||!k.fillText)return!1;switch(k.clearRect(0,0,j.width,j.height),k.textBaseline="top",k.font="600 32px Arial",a){case"flag":return k.fillText(f(55356,56826,55356,56819),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,56826,8203,55356,56819),0,0),c=j.toDataURL(),b!==c&&(k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447),0,0),c=j.toDataURL(),b!==c);case"emoji4":return k.fillText(f(55358,56794,8205,9794,65039),0,0),d=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55358,56794,8203,9794,65039),0,0),e=j.toDataURL(),d!==e}return!1}function e(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i,j=b.createElement("canvas"),k=j.getContext&&j.getContext("2d");for(i=Array("flag","emoji4"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
    </script>
    <style type="text/css">
    img.wp-smiley,
    img.emoji {
       display: inline !important;
       border: none !important;
       box-shadow: none !important;
       height: 1em !important;
       width: 1em !important;
       margin: 0 .07em !important;
       vertical-align: -0.1em !important;
       background: none !important;
       padding: 0 !important;
    }
    </style>
    <link href='//sqst.space/wp-content/plugins/a3-lazy-load/assets/css/jquery.lazyloadxt.spinner.css?ver=4.8.2' id='jquery-lazyloadxt-spinner-css-css' media='all' rel='stylesheet' type='text/css'>
    <script src='http://sqst.space/wp-includes/js/jquery/jquery.js?ver=1.12.4' type='text/javascript'>
    </script>
    <script src='http://sqst.space/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1' type='text/javascript'>
    </script>
    <link href='http://sqst.space/wp-json/' rel='https://api.w.org/'>
    <link href="http://sqst.space/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml">
    <link href="http://sqst.space/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml">
    <meta content="WordPress 4.8.2" name="generator">
    <link href="http://sqst.space/rsslatest.xml" rel="alternate" title="RSS" type="application/rss+xml">
</head>

<body>
    <div id="main_container">
        <div id="header">
            <div id="logo">
                <h1><a href="http://sqst.space">Office Design Ideas</a>
                </h1>


                <div class="desc">
                    Office Design and Remodel Inspiration
                </div>
            </div>
        </div>


        <div class="clear">
        </div>

@yield('content')

    <div id="footer">
        <div class="bottom_footer">
            <a href="http://sqst.space/contact-us/">Contact Us</a> | <a href="http://sqst.space/privacy-policy/">Privacy Policy</a> | <a href="http://sqst.space/term-of-service/">Terms Of Service</a><br>


            <div class="privacy">
                Copyright © 2017 Office Design Ideas. Some Rights Reserved.
            </div>
        </div>
    </div>
    <script type='text/javascript'>
    /* <![CDATA[ */
    var a3_lazyload_params = {"apply_images":"1","apply_videos":"1"};
    var a3_lazyload_params = {"apply_images":"1","apply_videos":"1"};
    /* ]]> */
    </script> 
    <script src='//sqst.space/wp-content/plugins/a3-lazy-load/assets/js/jquery.lazyloadxt.min.js?ver=1.8.2' type='text/javascript'>
    </script> 
    <script src='//sqst.space/wp-content/plugins/a3-lazy-load/assets/js/jquery.lazyloadxt.srcset.min.js?ver=1.8.2' type='text/javascript'>
    </script> 
    <script type='text/javascript'>
    /* <![CDATA[ */
    var a3_lazyload_extend_params = {"edgeY":"0"};
    var a3_lazyload_extend_params = {"edgeY":"0"};
    /* ]]> */
    </script> 
    <script src='//sqst.space/wp-content/plugins/a3-lazy-load/assets/js/jquery.lazyloadxt.extend.js?ver=1.8.2' type='text/javascript'>
    </script> 
    <script src='http://sqst.space/wp-includes/js/wp-embed.min.js?ver=4.8.2' type='text/javascript'>
    </script>
   <!-- Histats.com  START  (aync)-->
<script type="text/javascript">var _Hasync= _Hasync|| [];
_Hasync.push(['Histats.start', '1,3915070,4,0,0,0,00010000']);
_Hasync.push(['Histats.fasi', '1']);
_Hasync.push(['Histats.track_hits', '']);
(function() {
var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
hs.src = ('//s10.histats.com/js15_as.js');
(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
})();</script>
<noscript><a href="/" target="_blank"><img  src="//sstatic1.histats.com/0.gif?{{ $money['trackid'] }}&101" alt="" border="0"></a></noscript>
<!-- Histats.com  END  -->
</body>
</html>
