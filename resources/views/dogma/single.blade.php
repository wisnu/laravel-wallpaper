@extends('main')
@section('title')
	{{ ucwords(str_replace('-', ' ', $slug)) }}
@endsection

@section('meta')
<meta name="description" content="Download {{ ucwords($item->data[0]->content) }}">
<meta name="keywords" content="{{ implode(', ', array_slice($related, 0, 10)) }}">

<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Article",
  "image": "{{ url('image/'.$id.'/'.str_slug($item->data[0]->content)) }}",
  "name": "{{ ucwords(str_replace('-', ' ', $slug)) }} {{ ucwords($item->data[0]->content) }}",
  "publisher": "{{ $_SERVER['HTTP_HOST'] }}",
  "author": "{{ $_SERVER['HTTP_HOST'] }}",
  "headline": "{{ ucwords($item->data[0]->content) }}",
  "datePublished": "2017-05-31"
}
</script>
@endsection


@section('content')
<div class="content">
	<div class="article">
		<header class="main-header">
		<div id="header"><a href="{{ url('/') }}" title="{{ ucwords(str_replace('-', ' ', $slug)) }}" rel="nofollow"><h1>{{ $_SERVER['HTTP_HOST'] }}</h1></a></div>
		</header>

		<div class="header-text">
			<div class="crumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
				<span typeof="v:Breadcrumb"><a href="{{ url('/') }}" rel="v:url" property="v:title">Home</a></span> »
				<span typeof="v:Breadcrumb"></span><span class="crent"><a href="{{ url($slug) }}">{{ ucwords(str_replace('-', ' ', $slug)) }}</a></span> »
				<span typeof="v:Breadcrumb"></span><span class="crent">{{ ucwords($item->data[0]->content) }}</span>
			</div>
		</div>

		<div class="ads-top">
			<?=$money['responsiveAds']; //Ads ?>
		</div>
		<figure>
			<a href="{{ url('image/'.$id.'/'.str_slug($item->data[0]->content)) }}" title="{{ ucwords($item->data[0]->content) }}"><img width="100%" src="{{ url('image/'.$id.'/'.str_slug($item->data[0]->content)) }}" class="attachment-full wallpaper" alt="{{ ucwords($item->data[0]->content) }}" title="{{ ucwords($item->data[0]->content) }}" onerror="this.src='{{ url('image/'.$id.'/'.str_slug($item->data[0]->content)) }}';" id="exifviewer-img-0" exifid="{{$id}}" oldsrc="{{ url('image/'.$id.'/'.str_slug($item->data[0]->content)) }}"></a>
			<figcaption><h2>{{ ucwords($item->data[0]->content) }}</h2></figcaption>
		</figure>

		@foreach ($result->data as $data)
		<?php
			$tbId = explode('tbn:', $data->tbUrl);
			$tbId = $tbId[1];
			
			$imgId = explode('.', explode('/', $data->url)[4]);
			$imgId = $imgId[0];
		?>


		<div class="box">
			<a href="{{  str_slug($data->content).'_'.$imgId }}.html" class="th" title="{{ $data->content }}" alt="{{ $data->content }}"><img class="th" onerror="this.onerror=null;this.src='{{ url('thumbnail/'.$tbId.'/'.str_slug($data->content).'/223/149') }}';" src="{{ url('thumbnail/'.$tbId.'/'.str_slug($data->content).'/223/149') }}" width="225" height="100" title="{{ $data->content }}" alt="{{ $data->content }}" id="exifviewer-img-0" exifid="{{$tbId}}" oldsrc="{{ url('thumbnail/'.$tbId.'/'.str_slug($data->content).'/223/149') }}"></a>
			<h2>{{ $data->content }}</h2>
		</div>
		@endforeach


	</div>	<!-- Start Sidebar Logos -->


	<aside class="sidebar walleft1">
		<div id="logo2">
		</div>
	</aside>
	<!-- End Sidebar Logos -->


	<aside class="sidebar walleft1">
		<div class="sidebarmenunavigation">
		</div>
	</aside>


	<aside class="sidebar walleft1">

				<div id="sidebars" class="sidebar">
					<div class="sidebar_list">
						<ul class="rand-text">
							@foreach ($related as $rel)
								<ul class="popular-posts">
									<li><a href="{{ url(str_slug($rel)) }}" title="{{ ucwords($rel) }}">{{ ucwords($rel) }}</a><div class="sidebartextviews">» {{ rand(1000,3000) }}  views</div></li>
								</ul>
							@endforeach
						</ul>

						<div style="clear: both"></div>
					</div>
					<div class="ads_sidebar"><?=$money['responsiveAds']; //Ads ?><!--ads--></div>
				</div>
			</aside>
</div>
@endsection