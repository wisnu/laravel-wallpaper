@extends('main')
@section('title')
	{{ $title }}
@endsection

@section('meta')
<meta name="description" content="Download {{ ucwords(Config::get('money.niche')) }}">
<meta name="keywords" content="{{ implode(', ', array_slice($related, 0, 10)) }}">
@endsection


@section('content')
<div class="content">
	<div class="article">
		<header class="main-header">
		<div id="header"><a href="{{ url('/') }}" title="{{ $title }}" rel="nofollow"><h1>{{ $_SERVER['HTTP_HOST'] }}</h1></a></div>
		</header>

		<div class="header-text">
			<div class="crumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
				<span typeof="v:Breadcrumb"><a href="{{ url('/') }}" rel="v:url" property="v:title">Home</a></span> »
				<span typeof="v:Breadcrumb"></span><span class="crent">{{ $title }}</span>
			</div>
		</div>

		<div class="ads-top">
			<?=$money['responsiveAds']; //Ads ?>
		</div>

		@foreach ($result->data as $data)
		<?php
		$tbId = explode('tbn:', $data->tbUrl);
		$tbId = $tbId[1];
		
		if (isset($data->gid)) {
			$imgId = substr(base64_encode($data->gid), 0, 18);
		}
		else  {
			$imgId = substr(base64_encode($data->_id), 0, 18);
		}
		?>
		<div class="box">
			<a href="{{ url()->current() .'/'. str_slug($data->content).'_'.$imgId }}.html" class="th" title="{{ $data->content }}" alt="{{ $data->content }}"><img class="th" onerror="this.onerror=null;this.src='{{ url('thumbnail/'.$tbId.'/'.str_slug($data->content).'/223/149') }}';" src="{{ url('thumbnail/'.$tbId.'/'.str_slug($data->content).'/223/149') }}" width="225" height="100" title="{{ $data->content }}" alt="{{ $data->content }}" id="exifviewer-img-0" exifid="257141054" oldsrc="{{ url('thumbnail/'.$tbId.'/'.str_slug($data->content).'/223/149') }}"></a>
			<h2>{{ $data->content }}</h2>
		</div>
		@endforeach

	</div>	<!-- Start Sidebar Logos -->


	<aside class="sidebar walleft1">
		<div id="logo2">
		</div>
	</aside>
	<!-- End Sidebar Logos -->


	<aside class="sidebar walleft1">
		<div class="sidebarmenunavigation">
		</div>
	</aside>


	<aside class="sidebar walleft1">

				<div id="sidebars" class="sidebar">
					<div class="sidebar_list">
						<ul class="rand-text">
							@foreach ($related as $rel)
								<ul class="popular-posts">
									<li><a href="{{ url(str_slug($rel)) }}" title="{{ ucwords($rel) }}">{{ ucwords($rel) }}</a><div class="sidebartextviews">» {{ rand(1000,3000) }}  views</div></li>
								</ul>
							@endforeach
						</ul>

						<div style="clear: both"></div>
					</div>
					<div class="ads_sidebar"><?=$money['responsiveAds']; //Ads ?><!--ads--></div>
				</div>
			</aside>
</div>
@endsection