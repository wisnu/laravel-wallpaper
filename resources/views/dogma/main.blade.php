<!DOCTYPE html>

<html class="no-js" lang="en-US">
<head>
	<meta content="text/html; charset=utf-8" http-equiv="content-type">
	<meta charset="UTF-8"><!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
	<!--[if IE ]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <![endif]-->
	<link href="http://gmpg.org/xfn/11" rel="profile">
	<meta content="index,follow" name="robots">

	<title>
		@yield('title')
	</title>
	<link rel="canonical" href="{{ url()->current() }}" />
	<meta content="index,follow,imageindex" name="googlebot">
	<meta content="all,index,follow" name="robots">
	<meta content="all,index,follow" name="googlebot-Image">
	<meta content="width=device-width,initial-scale=1" name="viewport">
	<meta content="general" name="rating">
	<meta content="global" name="distribution">
	<meta content="en" name="language">
	<link href="{{ theme_url('favicon.ico') }}" rel="shortcut icon" type="image/x-icon">
	<link href="{{ theme_url('icon.png') }}" rel="apple-touch-icon">
	<link href="{{ theme_url('icon.png') }}" rel="icon" sizes="57x57">
	<meta content="{{ url('/') }}" name="application-name">
	<meta content="{{ url('/') }}" name="msapplication-tooltip">
	<meta content="{{ theme_url('icon.png') }}" name="msapplication-TileImage" title="favicon ie10">
	<meta content="yes" name="apple-mobile-web-app-capable">
	<meta content="black" name="apple-mobile-web-app-status-bar-style">
	<link href="{{ theme_url('style.css') }}" id="stylesheet-css" media="all" rel="stylesheet" type="text/css">
	<link href="{{ theme_url('responsive.css') }}" id="responsive-css" media="all" rel="stylesheet" type="text/css">
	<style id="stylesheet-inline-css" type="text/css">
	     	.content li {float: left;list-style-type: none;margin-bottom: 5px;margin-right: 6px;overflow: hidden;padding: 6px 9px;text-transform: capitalize;width: 29%;} .content li a{color:#fff;}
	.box{width:32%;display:block;float:left;text-align:center;background:#252525;height:150px;overflow:hidden;border-bottom:2px solid #111;margin:5px 2px;padding-top:5px}.box:hover{cursor:pointer;background:#000}.box h2,.box h3{font-size:11px;line-height:20px;margin:0;color:#666;font-weight:400}a.th{display:block;padding:5px;height:130px;overflow:hidden;margin:0 auto;box-sizing:border-box}a.th img{width:100%;height:auto;position:relative;top:50%;transform:translateY(-50%);-webkit-transform:translateY(-50%);-ms-transform:translateY(-50%)}.fb_iframe_widget,.fb_iframe_widget span,.fb_iframe_widget span iframe[style]{min-width:100%!important;width:100%!important}.share-box{width:90%;max-width:718px;background:#f9f9f9;border-radius:3px;margin:5px auto;border:1px solid #ddd;padding:10px 5px}.share-box span{color:#666;width:85px;display:block;float:left;text-align:right}input.share-input{margin-left:10px;width:80%;max-width:650px}.ads-bottom,.ads-top{width:100%;max-width:728px;height:auto;margin:0 auto;overflow:hidden;box-sizing:border-box}.ads-right{width:100%;height:auto;padding:10px 0;overflow:hidden;box-sizing:border-box}ul.rand-text li{width:100%;box-sizing:border-box;list-style-type:none}ul.rand-text h3{text-transform:uppercase;font-size:10px;margin-bottom:0;height:12px;overflow:hidden}ul.rand-text p{color:#999;margin-top:0}.insearch{background:#111;border:none;font-weight:700;color:#fff;letter-spacing:1px;font-size:12px;float:right;margin:0 0 5px 10px;width:100%;padding:6px;box-sizing:border-box}.menu li{list-style:none;display:inline-block;margin-left:15px;text-transform:uppercase;font-weight:700;letter-spacing:1px;font-size:10px}.menu{margin-top:5px}.menu li a{color:#BBB}.menu li a:hover{color:#fff}.content ul{text-align:justify;color:#ccc;margin-left:30px}.bf{color:#CCC;text-align:center;background:rgb(51,51,51);font-size:11px;margin-top:20px;letter-spacing:2px;text-transform:uppercase;padding:15px}.bf a{color:rgb(199,199,199)}.bf a:hover{color: rgb(0, 211, 219)}img.wallpaper{min-height:300px}#pagination{width:100%;margin-top:15px;clear:both;text-align:center}#pagination a,#pagination span{padding:6px 8px;margin:1px;line-height:30px}#pagination a:hover{text-decoration:none;background:#000;color:#bbb}#pagination b{background:#000;color:#bbb;padding:6px 8px;margin:2px}@media screen and (max-width:960px){#cl,#sb,ul.rand-text{max-width:960px}ul.rand-text li{width:49%;float:left;padding-right:10px;box-sizing:border-box}.ads-right{height:90px}}@media screen and (max-width:680px){.box{height:120px}.box a{height:100px}}@media screen and (max-width:500px){.box{width:48%}}@media screen and (max-width:323px){.box{width:99%;height:150px}.box a{height:130px}ul.rand-text li{width:100%;float:none}}
	#pagination{width:100%;max-width:728px;margin-top:15px;clear:both;text-align:center;float:left;display:inline-block;}
	#pagination a,#pagination span{border:1px solid #fff;padding:6px 8px;margin:1px;line-height:30px}
	#pagination a:hover{text-decoration:none;background:#000;color:#bbb}
	#pagination b{background:#000;color:#bbb;padding:6px 8px;margin:2px}
	input#author:focus, input#email:focus, input#url:focus, #commentform textarea:focus, .widget .wpt_widget_content #tags-tab-content ul li a { border-color:#029890;}
	       .widget h3, .frontTitle, .footer-navigation ul, .article, .description_images_bottom, .footer-navigation a, .sidebarpostviews,.dashed,.dashedpost,.dashedattach, .dashedsocial, .total-comments, #respond h4 { border-color:#029890;}
	       a:hover, .menu .current-menu-item > a, .menu .current-menu-item, .current-menu-ancestor > a.sf-with-ul, .current-menu-ancestor, footer .textwidget a, .single_post a, #commentform a, .copyrights a:hover, a, footer .widget li a:hover, .menu > li:hover > a, .single_post .post-info a, .post-info a, .readMore a, .reply a, .fn a, .carousel a:hover, .single_post .related-posts a:hover, .textwidget a, footer .textwidget a, .sidebar.walleft1 a:hover, .sidebar.walleft2 a:hover, .smallbutton a:hover, .related-posts2 li a:hover, .featured-cat,.infoviews { color:#029890; }  
	       .nav-previous a, .nav-next a, .header-button, .sub-menu, #commentform input#submit, .tagcloud a:hover, #tabber ul.tabs li a.selected, .wall-subscribe input[type='submit'],.pagination a, .widget .wpt_widget_content #tags-tab-content ul li a, .latestPost-review-wrapper, .sidebarmenunavigation a:hover { background-color:#029890; color: #fff; }

	       
	           
	</style>
</head>

<body class="home blog main" datasqstyle="{&quot;paddingTop&quot;:null}" datasquuid="e94e59ab-cdd3-4586-adfb-904d7c2b0d58" id="blog" style="padding-top: 40px;">
	<div class="main-container">
		<div class="home-page" id="page">
@yield('content')
		</div>
		<!--#page-->


		<footer>
		</footer>


		<div class="copyrights">

			<div class="footer-navigation">
				<ul class="menu">
				</ul>
			</div>


			<div class="row" id="copyright-note">
				<div class="copyright-left-text">
					<div class="nav">
						<ul>
							<li>
								<a href="{{ url('/') }}" rel="follow">Home</a>
							</li>


							<li>
								<a href="{{ url('page/contact.html') }}" rel="nofollow">Contact</a>
							</li>


							<li>
								<a href="{{ url('page/dmca.html') }}" rel="nofollow">DMCA</a>
							</li>
							
							<li>
								<a href="{{ url('page/toc.html') }}" rel="nofollow">Terms</a>
							</li>


							<li>
								<a href="{{ url('page/privacy-policy.html') }}" rel="nofollow">Privacy Policy</a>
							</li>
						</ul>
					</div>
					<!--Aamiin-->
				</div>


				<div class="copyright-text">
					Copyright &copy; {{date('Y')}} <a href="{{ url('/') }}" rel="nofollow" title="All About {{ $_SERVER['HTTP_HOST'] }} for Resume Refference">{{ $_SERVER['HTTP_HOST'] }}</a> - All About {{ $_SERVER['HTTP_HOST'] }} for Your Resume Refference
				</div>
			</div>
			<!--end copyrights-->
		</div>
	</div>
   <!-- Histats.com  START  (aync)-->
<script type="text/javascript">var _Hasync= _Hasync|| [];
_Hasync.push(['Histats.start', '1,3915070,4,0,0,0,00010000']);
_Hasync.push(['Histats.fasi', '1']);
_Hasync.push(['Histats.track_hits', '']);
(function() {
var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
hs.src = ('//s10.histats.com/js15_as.js');
(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
})();</script>
<noscript><a href="/" target="_blank"><img  src="//sstatic1.histats.com/0.gif?{{ $money['trackid'] }}&101" alt="" border="0"></a></noscript>
<!-- Histats.com  END  -->
</body>
</html>