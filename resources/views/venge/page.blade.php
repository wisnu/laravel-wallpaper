@extends('main')
@section('title')
	Free Download {{ ucwords(Config::get('money.niche')) }}
@endsection

@section('content')
<div id="main-wrapper">
				<div class="container">
					<div class="row">
						<div id="main-container" class="col-md-9">
							<ol class="breadcrumb">
								<li><a href="{{ route('home') }}">Home</a></li>
								<li><a href="{{ url()->current() }}">{{ ucwords($page) }}</a></li>
							</ol><!--breadcrumb-->
							<article class="post-content">
								<h2 class="single-article-title">{{ $page }}</h2><!--post-title-->
								<div class="post-entry push_bottom_30">
									<p><?=$content;?></p>
								</div><!--post-entry-->
					        </article><!--post-content-->
						</div><!--main container -->
						<aside class="sidebar col-md-3">
							<div class="widget widget_search push_bottom_30">
								<form role="search" method="get" action="archive.html" class="search-form">
									<div class="form-group">
										<input type="text" name="s" value="Type a keyword and hit enter ....." onfocus="if (this.value == 'Type a keyword and hit enter .....') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Type a keyword and hit enter .....';}" class="form-control search-widget-input">
									</div><!-- from group -->
								</form><!-- search form -->
							</div><!-- Search widget-->


							<div class="widget widget-tabbed push_bottom_30" id="widget_tabs">
								<div class="panel-group">
									<div class="tab-content">
										<div class="tab-pane box-content row active" id="recent_widget_tabs">
											<article class="article other-article side-article col-md-12">
												@foreach ($related as $rel)
												<h4 class="article-title"><a href="{{ url(str_slug($rel)) }}" title="{{ ucwords($rel) }}" rel="bookmark">{{ ucwords($rel) }}</a></h4>
												@endforeach
											</article>
										</div>

@endsection